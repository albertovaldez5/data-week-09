- [SQL Alchemy](#sql-alchemy)
  - [Basics](#basics)
  - [SQL Queries and Pandas](#sql-queries-and-pandas)
  - [Object Relational Mapper (ORM)](#object-relational-mapper-orm)
  - [Queries with Python objects](#queries-with-python-objects)
- [Inspecting the Datadase](#inspecting-the-datadase)
  - [Inspecting the Engine](#inspecting-the-engine)
  - [Using MetaData and Tables](#using-metadata-and-tables)
- [Working with Dates](#working-with-dates)
  - [Filtering dates using func](#filtering-dates-using-func)
  - [Filtering dates using datetime](#filtering-dates-using-datetime)
- [Boilerplate](#boilerplate)
  - [Basic Imports](#basic-imports)
  - [SQL Engine](#sql-engine)
  - [Classes and Metadata](#classes-and-metadata)
- [Complex SQL Queries](#complex-sql-queries)
  - [Invoice Totals](#invoice-totals)
  - [Postal Codes from USA](#postal-codes-from-usa)
  - [Item Totals](#item-totals)
  - [Descending by Total](#descending-by-total)



<a id="sql-alchemy"></a>

# SQL Alchemy

SQLAlchemy helps us access SQL databases from Python.


<a id="basics"></a>

## Basics

In order to access a database we must create an engine, which will be a Python object that will handle all connections to the database. We are going to give it a path to the local SQLite database.

```python
# SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy import inspect

database_path = "../resources/Census_Data.sqlite"
engine = create_engine(f"sqlite:///{database_path}")
inspector = inspect(engine)
print(inspector.get_table_names())
```

    ['Census_Data']

We can use `pandas` to convert data fetched by the engine, in this case the column data.

```python
import pandas as pd

columns = inspector.get_columns('Census_Data')
df = pd.DataFrame(columns)
print(df.head())
```

                   name     type  nullable default autoincrement  primary_key
    0         CityState     TEXT      True    None          auto            0
    1              city     TEXT      True    None          auto            0
    2             state     TEXT      True    None          auto            0
    3        Population  INTEGER      True    NULL          auto            0
    4  White Population  INTEGER      True    NULL          auto            0


<a id="sql-queries-and-pandas"></a>

## SQL Queries and Pandas

The engine can create a connection, which we can use to make queries to a database.

```python
conn = engine.connect()
census_data_df = pd.read_sql("select * from Census_Data", conn)
print(census_data_df.head())
```

             CityState         city state  Population  ...  Median Gross Rent  Median Home Value        lat         lng
    0      HOUSTON, TX      HOUSTON    TX     3061887  ...         956.708333        178233.6842  29.775734  -95.414548
    1      CHICAGO, IL      CHICAGO    IL     2702091  ...        1119.928571        264739.2857  41.867838  -87.673440
    2     BROOKLYN, NY     BROOKLYN    NY     2595259  ...        1261.783784        605743.2432  40.652805  -73.956528
    3  LOS ANGELES, CA  LOS ANGELES    CA     2426413  ...        1201.766667        557115.0000  34.042209 -118.303468
    4        MIAMI, FL        MIAMI    FL     1820704  ...        1260.833333        243279.6296  25.760268  -80.298511
    
    [5 rows x 31 columns]

Let&rsquo;s say we want to make a query like the following one.

```sqlite
SELECT city, state, Population
FROM Census_Data
WHERE "Household Income" > 100000
LIMIT 5;
```

    city        state       Population
    ----------  ----------  ----------
    ALEXANDRIA  VA          342825
    FREMONT     CA          225250
    ARLINGTON   VA          223446
    NAPERVILLE  IL          163550
    FAIRFAX     VA          158409

One way to do it is to use Pandas to filter the data with basic `loc` and indexing.

```python
r = census_data_df.loc[census_data_df["Household Income"] > 100000][["city", "state", "Population"]]
print(r.head())
```

               city state  Population
    82   ALEXANDRIA    VA      342825
    142     FREMONT    CA      225250
    145   ARLINGTON    VA      223446
    224  NAPERVILLE    IL      163550
    232     FAIRFAX    VA      158409


<a id="object-relational-mapper-orm"></a>

## Object Relational Mapper (ORM)

SQLAlchemy uses an ORM to translate the database data into Python objects/classes.

It uses SQLAlchemy Core get all the relations and values in the database and give it to the ORM to translate.

Python SQL toolkit and ORM.

1.  `automap_base`
2.  `Session`
3.  `create_engine`
4.  `inspect`

We will import the functions to create the engine, a base and a session.

```python
import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy.inspection import inspect
from sqlalchemy import create_engine

db_path = "../resources/demographics.sqlite"
engine = create_engine(f"sqlite:///{db_path}")
Base = automap_base()
Base.prepare(engine, reflect=True)
Demographics = Base.classes.demographics
print(Base.classes.keys())
```

    ['demographics']


<a id="queries-with-python-objects"></a>

## Queries with Python objects

We can start a session and perform a `query`. Then use `group_by` and count.

```python
session = Session(engine)
r = session.query(Demographics.location).limit(5).all()
print(r)
count = session.query(Demographics).group_by(Demographics.location).count()
print(f"Unique locations: {count}")
```

    [('South Dakota',), ('Delaware',), ('South Dakota',), ('Louisiana',), ('West Virginia',)]
    Unique locations: 5


<a id="inspecting-the-datadase"></a>

# Inspecting the Datadase

We can use the `inspect` function to get more info. about our database.


<a id="inspecting-the-engine"></a>

## Inspecting the Engine

We can get the columns from the database and their info in a few different ways.

```python
inspector = inspect(Demographics)
print("Columns:", [c.key for c in inspector.mapper.column_attrs])
inspector = inspect(engine)
for c in inspector.get_columns("demographics"):
    print(c["name"], c["type"])
```

    Columns: ['id', 'age', 'height_meter', 'weight_kg', 'children', 'occupation', 'academic_degree', 'salary', 'location']
    id INTEGER
    age INTEGER
    height_meter FLOAT
    weight_kg FLOAT
    children INTEGER
    occupation TEXT
    academic_degree TEXT
    salary INTEGER
    location TEXT


<a id="using-metadata-and-tables"></a>

## Using MetaData and Tables

We can also use Metadata and Table to get the attributes of the tables in our database.

```python
from sqlalchemy import MetaData, Table

metadata = MetaData()
table = Table('demographics', metadata, autoload=True, autoload_with=engine)
print(f"Columns:\n{[c for c in table.columns][:3]}")
print([c for c in table.columns.keys()][:3])
```

    Columns:
    [Column('id', INTEGER(), table=<demographics>, primary_key=True, nullable=False), Column('age', INTEGER(), table=<demographics>), Column('height_meter', FLOAT(), table=<demographics>)]
    ['id', 'age', 'height_meter']


<a id="working-with-dates"></a>

# Working with Dates

When working with SQL Alchemy, we can process datetime methods with Python because we are working with Python object.

Import `func` to use functions from SQL Alchemy.

```python
import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, inspect, func

db_path = "../resources/dow.sqlite"
engine = create_engine(f"sqlite:///{db_path}")
Base = automap_base()
Base.prepare(engine, reflect=True)
print(Base.classes.keys())
Dow = Base.classes.dow
print(Dow)
session = Session(engine)
r = session.query(Dow.date, Dow.stock).all()
print(r[:5])
```

    ['dow']
    <class 'sqlalchemy.ext.automap.dow'>
    [('2011-01-07', 'AA'), ('2011-01-14', 'AA'), ('2011-01-21', 'AA'), ('2011-01-28', 'AA'), ('2011-02-04', 'AA')]


<a id="filtering-dates-using-func"></a>

## Filtering dates using func

We can use `func.strftime` to convert any date-like string into a date and then compare them.

```python
def query_date(date: str, frmt: str, limit: int = 5) -> list:
    """Get date, stock, open_price, close_price by date.

    Args:
        date (str): Date as string.
        frmt (str): Date format filter, %d, %m, etc.
        limit (int): Optional.

    Returns:
        (list): query result.
    """
    return (
        session
        .query(Dow.date, Dow.stock, Dow.open_price, Dow.close_price)
        .filter(func.strftime(frmt, Dow.date) == date)
        .limit(limit)
        .all()
    )

print("By day:")
for date in query_date("14", "%d"):
    print(date)
print("By month:")
for date in query_date("06", "%m"):
    print(date)
```

    By day:
    ('2011-01-14', 'AA', 16.71, 15.97)
    ('2011-01-14', 'AXP', 44.2, 46.25)
    ('2011-01-14', 'BA', 69.42, 70.07)
    ('2011-01-14', 'BAC', 14.17, 15.25)
    ('2011-01-14', 'CAT', 93.21, 94.01)
    By month:
    ('2011-06-03', 'AA', 16.73, 15.92)
    ('2011-06-10', 'AA', 15.92, 15.28)
    ('2011-06-17', 'AA', 15.29, 14.72)
    ('2011-06-24', 'AA', 14.67, 15.23)
    ('2011-06-03', 'AXP', 51.39, 49.28)


<a id="filtering-dates-using-datetime"></a>

## Filtering dates using datetime

We can use the Python `datetime` module instead.

```python
import datetime as dt

date = dt.datetime(2011, 5, 31)
r = (
    session
    .query(Dow.high_price - Dow.low_price)
    .filter(Dow.date > date)
    .filter(Dow.stock == "IBM")
    .all()
)
for i in r:
    print(i)
```

    (5.759999999999991,)
    (3.0900000000000034,)
    (3.579999999999984,)
    (3.219999999999999,)


<a id="boilerplate"></a>

# Boilerplate

This is a review of all the pieces we used for making queries.


<a id="basic-imports"></a>

## Basic Imports

We are importing all the individual pieces that will help us setup our database interactions.

The first four will help us make a connection to the database use the ORM. The last three will help us make queries. See the SQLAlchemy documentation <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> for more info.

```python
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.inspection import inspect
from sqlalchemy.orm import Session
from sqlalchemy import MetaData, Table
from sqlalchemy import func
```


<a id="sql-engine"></a>

## SQL Engine

We are setting up a database specific path with other boilerplate SQL engine and Base setup. The Base helps us convert the SQL tables into Python Classes with the SQLAlchemy ORM and the engine will help us make a connection to the database.

```python
db_path = "../resources/chinook.sqlite"
engine = create_engine(f"sqlite:///{db_path}")
Base = automap_base()
Base.prepare(engine, reflect=True)
inspector = inspect(engine)
print(Base.classes.keys())
```

    ['albums', 'artists', 'customers', 'employees', 'genres', 'invoice_items', 'invoices', 'tracks', 'media_types', 'playlists']


<a id="classes-and-metadata"></a>

## Classes and Metadata

Now we go one level lower and start dealing with database specific table names.

```python
Invoices = Base.classes.invoices
Items = Base.classes.invoice_items
metadata = MetaData()
invoices = Table('invoices', metadata, autoload=True, autoload_with=engine)
invoice_items = Table('invoice_items', metadata, autoload=True, autoload_with=engine)
print(invoices.columns.keys())
```

    ['InvoiceId', 'CustomerId', 'InvoiceDate', 'BillingAddress', 'BillingCity', 'BillingState', 'BillingCountry', 'BillingPostalCode', 'Total']


<a id="complex-sql-queries"></a>

# Complex SQL Queries

The session will help us do a transaction between Python and the SQL database, sort of a subset of the engine which is already connected to the database URL/path.

The `filterwarnings` setting is just for dealing with data type conversion from sqlite.

```python
import warnings
warnings.filterwarnings('ignore')
session = Session(engine)
print(session)
```

    <sqlalchemy.orm.session.Session object at 0x104058d90>


<a id="invoice-totals"></a>

## Invoice Totals

Descending Invoice Totals for Each Billing Country

```python
r = (
    session.query(Invoices.BillingCountry, Invoices.Total)
    .group_by(Invoices.BillingCountry)
    .order_by(func.sum(Invoices.Total).desc())
    .limit(10)
    .all()
)
for i in r:
    print(i)
```

    ('USA', Decimal('13.86'))
    ('Canada', Decimal('8.91'))
    ('France', Decimal('1.98'))
    ('Brazil', Decimal('8.91'))
    ('Germany', Decimal('1.98'))
    ('United Kingdom', Decimal('8.91'))
    ('Czech Republic', Decimal('8.91'))
    ('Portugal', Decimal('1.98'))
    ('India', Decimal('3.96'))
    ('Chile', Decimal('1.98'))


<a id="postal-codes-from-usa"></a>

## Postal Codes from USA

Billing Postal Codes from the USA ordered by Billing Country.

```python
r = (
    session.query(Invoices.BillingPostalCode)
    .where(Invoices.BillingCountry == "USA")
    .distinct()
    .order_by(Invoices.BillingCountry)
    .limit(10)
    .all()
)
for i in r:
    print(i)
```

    ('2113',)
    ('94043-1351',)
    ('98052-8300',)
    ('95014',)
    ('89503',)
    ('53703',)
    ('85719',)
    ('76110',)
    ('84102',)
    ('32801',)


<a id="item-totals"></a>

## Item Totals

Item Totals (sum(UnitPrice \* Quantity)) for the USA.

```python
r = (
    session.query(
        func.sum(Items.UnitPrice * Items.Quantity)
    )
    .join(
        Invoices,
        Items.InvoiceId==Invoices.InvoiceId
    )
    .where(
        Invoices.BillingCountry == "USA"
    )
    .all()
)
print(r[0])
```

    (Decimal('523.0600000000'),)


<a id="descending-by-total"></a>

## Descending by Total

Previous Query plus Sort Descending by Total.

```python
r = (
    session.query(
        Invoices.BillingPostalCode,
        func.sum(Items.UnitPrice * Items.Quantity)
    )
    .filter(
        Invoices.InvoiceId == Items.InvoiceId
    )
    .filter(
        Invoices.BillingCountry == "USA"
    )
    .group_by(
        Invoices.BillingPostalCode
    )
    .order_by(
        func.sum(Items.UnitPrice * Items.Quantity).desc()
    )
    .all()
)
for i in r:
    print(i)
```

    ('76110', Decimal('47.6200000000'))
    ('60611', Decimal('43.6200000000'))
    ('84102', Decimal('43.6200000000'))
    ('53703', Decimal('42.6200000000'))
    ('94040-111', Decimal('39.6200000000'))
    ('32801', Decimal('39.6200000000'))
    ('98052-8300', Decimal('39.6200000000'))
    ('95014', Decimal('38.6200000000'))
    ('10012-2612', Decimal('37.6200000000'))
    ('2113', Decimal('37.6200000000'))
    ('85719', Decimal('37.6200000000'))
    ('89503', Decimal('37.6200000000'))
    ('94043-1351', Decimal('37.6200000000'))

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://docs.sqlalchemy.org/en/13/>
