# 1. Import Flask
from flask import Flask

# 2. Create an app
app = Flask(__name__)

# 3. Define index route
@app.route("/")
def home():
    print("Hello")
    return "Welcome"

# 4. Define the about route
@app.route("/about")
def about():
    print("This is about")
    return "Welcome to about"

# 5. Define the contact route
@app.route("/contact")
def contact():
    print("Contact pls")
    return "Contact page"

# 6. Define main behavior
if __name__ == "__main__":
    app.run(debug=True)